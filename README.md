# Project Overview #
These tasks are designed to assess the skills that will be required on a daily basis in the role that we are offering. If any clarifications are required please ask.  

As a part of this task please keep a record of time spent on different aspects of the page as well as any issues that you encounter.  If you have any comments about the way the project is set up or things that you would do differently, feel free to let us know.  

We don’t expect you to necessarily finish all tasks on the 'extended' list.  If you feel there is other functionality or features that you would like to add, please feel free.

Submit your work either as a pull request or as a zip file over email.

## Tasks ##
### HTML Conversion ###
* The development task is to create the page as designed in the file index.psd
* If you don’t have Adobe Photoshop available then please use the index.jpg as your reference.
* Consider aspects such as SEO in your development as well as mobile responsiveness.  Feel free to include and use a front-end framework if you like
* The font used is a Google Web Font called Roboto Condensed

### Extended functionality ###
* Using Slick (http://kenwheeler.github.io/slick/) convert the large image into a slider/rotator that changes between the black and white versions of the image
* Include one or more social feeds or sharing options on the page
* Create 2-3 more pages, eg. ‘About Us’, ‘Portfolio’.  Using PHP or some other templating system, break the pages up into reusable sections including a navigation menu
* Include a PHP call to the mail function that sends an email to tech@themarkagency.com.au .  Connect this to a button or some other control
* Include a database connection to a database with the following:
 * Name: tma
 * Username: tma
 * Password: m4rk4g3